<form role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
	<div class="pgl-search">
		<input type="text" class="form-control" value="<?php echo get_search_query(); ?>" name="s" id="s" placeholder="Найти в каталоге"/>
		<input type="submit" id="searchsubmit" value="<?php echo esc_attr__( 'Search', 'nast' ); ?>" />
		<i class="fa fa-search"></i>
	</div>
</form>