jQuery(document).ready(function() {

	var FIXED_HEADER_HEIGHT = 50;

	/*
	 Smooth scrolling
	 */

	jQuery('a[href=#table1]').click(function(){

			jQuery('html, body').stop().animate({
		        scrollTop: jQuery( '#table1' ).offset().top - FIXED_HEADER_HEIGHT
		    }, 500);
		    return false; 

	});

	/*
	replace Cart to Корзина
	*/
	jQuery('.uk-panel .widget-title').text('Корзина');



	/*------------------------
		add to cart button
		 when nothing selected	

		Global button
		 */
	jQuery('.globalcartbtn').click(function(){//.single_add_to_cart_button

		jQuery('.js-var-form-error').hide();

		// var val = jQuery(this).parent().parent().find('.qtycol .var_quantity').val();
		var val = 0;
		var arr_var_quantity = jQuery('input[name=var_quantity]');
		jQuery.each(arr_var_quantity , function (){
			val += Number(jQuery(this).val());
		});

		if ( val <= '0'){
			if( !jQuery('#single-product .summary .js-var-form-error').length ){
				jQuery('#single-product .summary').append('<p class="js-var-form-error">Введите количество</p>');
			} else {
				jQuery('.js-var-form-error').show().text('Введите количество');
			}
			
		}
	})


	/*
		in-row buttons
	*/
	jQuery('.single_add_to_cart_button').click(function(){

		jQuery('.js-var-form-error').hide();
		jQuery('.js-var-form-error-inline').remove();

		var val = jQuery(this).parent().parent().parent().find('.qtycol input[name=var_quantity]').val();

		if ( val <= '0'){
			jQuery(this).parent().parent().append('<p class="js-var-form-error-inline">Введите количество</p>');
		}
	})

	/* 
		Inputs
	*/

	jQuery('input[name=var_quantity]').on('input change',function(){
		if (jQuery(this).val() > '0' ){
			jQuery(this).parent().parent().addClass('selected');
		} else {
			jQuery(this).parent().parent().removeClass('selected');
		}

		jQuery('.js-var-form-error').hide();
		jQuery('.js-var-form-error-inline').remove();
	})

	// ------------------------

	/* ------------------------
		Run fancybox
	----------------------------*/
	jQuery('.fancybox').fancybox();

	/*----------------------------
		корзина:
		минимальное ограничение
	----------------------------*/
	function checkMinSumm(){
		var ORDER_MIN = 7000;
		var order_total = parseInt( jQuery('.shop_table .order-total .amount').text().replace('.','').replace(',','') );
		if (order_total < ORDER_MIN) {
			jQuery('.shop_table.cart .checkout-button').addClass('disabled');
		};
	}
	checkMinSumm();

	jQuery('.shop_table.cart .checkout-button.disabled').on('click',function(){
		jQuery(this).attr('href','#');
	});
	//also see \wp-content\plugins\woocommerce-ajax-cart\

	/*-------------------
		homepage category cards
	-------------------*/
	jQuery('.caption-bar .wpb_singleimage_heading').click(function(){
		console.log('h22');
		jQuery(this).next('a')[0].click();
	});

	/*My Deer*/
    var clicked_times = 0;
    jQuery('#christmas_tree').on('click', function(){
        clicked_times++;
        if (clicked_times == 2 ){
            jQuery('#ny-deer-hello').animate({
            	opacity: '1',
		        bottom: '20px'
		    }).animate({
            	opacity: '0',
		        bottom: '0'
		    });
        }
    })

})