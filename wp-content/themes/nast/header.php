<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<?php pgl_wp_set_meta(); ?>
    <meta name="yandex-verification" content="7292da0a41f2ec1f" />
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <?php global $theme_option; ?>
    <?php do_action('pgl_before_wrapper'); ?>
    <!-- START Wrapper -->
	<div class="pgl-wrapper<?php echo apply_filters('pgl_style_layout',''); ?>">
        <div class="wrapper-inner">
    		<!-- HEADER -->
            <?php get_template_part( 'templates/header/header', apply_filters( 'pgl_header_layout', 1 ) ); ?>
    		<!-- //HEADER -->
            <?php //apply_filters( 'pgl_header_layout', 1 ) ?>