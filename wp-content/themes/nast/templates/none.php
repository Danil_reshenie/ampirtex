
<div class="page-found clearfix">
	<h2 class="entry-title"><?php echo __( 'Ничего не найдено', 'nast' ); ?></h2>
</div>
<article class="wrapper">
	<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

	<p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'nast' ), admin_url( 'post-new.php' ) ); ?></p>

	<?php elseif ( is_search() ) : ?>

	<p><?php _e( 'К сожалению мы не нашли ничего по вашему запросу. Попробуйте поиск по другому ключевому слову.', 'nast' ); ?> Или вернитесь <a href="http://ampirtex37.ru/catalog/">в каталог</a>.</p>
	<?php get_search_form(); ?>

	<?php else : ?>

	<p><?php _e( 'Ничего не найдено по вашему критерию', 'nast' ); ?></p>
	<?php get_search_form(); ?>

	<?php endif; ?>
</article>