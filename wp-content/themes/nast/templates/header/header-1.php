<?php
    global $theme_option,$woocommerce;
    $login_url = wp_login_url();
    $register_url = wp_registration_url();
    $account_link = get_edit_profile_url();
    $myaccount_page_id = get_option( 'woocommerce_myaccount_page_id' );
    if ( $myaccount_page_id ) {
        $login_url = get_permalink( $myaccount_page_id );
        if ( get_option( 'woocommerce_force_ssl_checkout' ) == 'yes' ) {
            $login_url = str_replace( 'http:', 'https:', $login_url );
        }
        if( get_option( 'woocommerce_enable_myaccount_registration' ) == 'yes' ){
            $register_url = $login_url;
        }
        $account_link = get_permalink($myaccount_page_id);
    }



    function isChristmastime() {
        $month = date('n');
        $day = date('j');
        return ($month == 12 && $day >= 11) || ($month == 1 && $day <= 15);
    }
?>
<header id="pgl-header" class="pgl-header">
    <div id="header-topbar">
        <div class="container">
            <div class="inner-topbar row">
                <div class="col-sm-6 topbar-1">
                    <?php if(isset($theme_option['header_topbar_text']) && $theme_option['header_topbar_text']!='') echo $theme_option['header_topbar_text']; ?>
                    <?php if( isset($theme_option['header-is-switch-language']) && $theme_option['header-is-switch-language'] ){ ?>
                    <div class="language-filter pull-left">
                        <?php echo pgl_language_flags(); ?>
                    </div>
                    <?php } ?>
                </div>
                <div class="col-sm-6 topbar-2">
                    <div class="header-toplinks">
                        <ul class="links">
                            <?php if(is_user_logged_in()){ ?>
                                <li class="first">
                                    <a class="account" href="<?php echo esc_url($account_link); ?>" title="Account">
                                        <?php echo __('Account','nast'); ?>
                                    </a>
                                </li>
                            <?php } ?>
                            <?php if(PLG_WOOCOMMERCE_ACTIVED){ ?>
                                <?php if ( class_exists( 'YITH_WCWL_UI' ) ) { global $yith_wcwl; ?>
                                <li>
                                    <a class="wishlist" href="<?php echo esc_url($yith_wcwl->get_wishlist_url()); ?>" title="<?php _e('Wishlist','nast'); ?>">
                                        <?php echo __('Wishlist','nast'); ?>
                                    </a>
                                </li>
                                <?php } ?>
                                <li>
                                    <a class="checkout" href="<?php echo esc_url($woocommerce->cart->get_checkout_url()); ?>" title="Checkout">
                                        <?php echo __('Checkout','nast'); ?>
                                    </a>
                                </li>
                            <?php } ?>
                            <?php if(!is_user_logged_in()){ ?>
                                <li class="last">
                                    <a class="login" href="<?php echo esc_url($login_url); ?>" title="<?php _e('Log In','nast'); ?>">
                                        <?php echo __('Log In','nast'); ?>
                                    </a>
                                    <span>/</span>
                                    <a class="register" href="<?php echo esc_url($register_url); ?>" title="<?php _e('Customer Register','nast'); ?>">
                                        <?php echo __('Register','nast'); ?>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-content">
        <?php 
        if (isChristmastime()) : ?>
            <div style="position: absolute; left: 0; top: -23px; right: 0; background: url('<?= site_url()?>/wp-content/themes/nast/images/girlanda.png'); height: 50px; background-size: contain;"></div>
            <div id="christmas_tree" style="position: absolute; left: 0; bottom: 40px; background: url('<?= site_url()?>/wp-content/themes/nast/images/elka.png'); height: 100px; width: 100px; background-size: contain;background-repeat: no-repeat;"></div>
            <div id="ny-deer-hello" style="position: absolute; left: 40%; right: 0; bottom: 0px; background: url('<?= site_url()?>/wp-content/themes/nast/images/ny-deer.png'); height: 160px; width: 160px; opacity: 0; background-size: contain;background-repeat: no-repeat; "></div>
        <?php endif;?>
        
        <div class="container">
            <div class="header-content-inner">
                <div class="row">
                    <div class="col-md-2 logo">
                        <?php do_action('pgl_set_logo'); ?>
                    </div>
                    <div class="col-md-10 hidden-xs content-navigation">
                        <?php 
                            //if( isset($theme_option['header_text']) && $theme_option['header_text']!='' )
                                //echo $theme_option['header_text'];

                        //custom: ("header text" from options)
                        ?>
                        <div class="header-static">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="box box1">
                                    <div class="title boxvitol" align="left">
                                        <span class="text kontacts1">Производство и оптовая продажа текстиля</span><span class="des kontacts3">из Иваново с доставкой по всей России</span>
                                        <span style="color: #e24545;font-weight: bold;padding-top: 1em;display: block;">Минимальный заказ от 7 000 р.</span>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="box box1">
                                        <a href="/o-kompanii" class="title boxvitol2" style="text-decoration: none; display: block;">
                                            <svg class="alignleft" width="36" height="36" viewBox="0 0 512 512" style="margin: 0 10px 0 0;">
                                                <path d="M477.867,422.4h-25.6V268.8c0-3.029-0.094-6.042-0.265-9.028c-2.569-44.467-25.293-84.932-62.344-111.01     c-22.281-15.701-47.718-24.951-73.924-27.46V89.6h8.533h17.067c4.71,0,8.533-3.814,8.533-8.533s-3.823-8.533-8.533-8.533H332.8     V38.4h8.533c4.71,0,8.533-3.814,8.533-8.533s-3.823-8.533-8.533-8.533h-17.067h-34.133h-17.067c-4.71,0-8.533,3.814-8.533,8.533     s3.823,8.533,8.533,8.533h8.533v34.133h-8.533c-4.71,0-8.533,3.814-8.533,8.533s3.823,8.533,8.533,8.533h17.067h8.533v31.036     c-13.688,0.23-27.452,2.27-40.951,6.246l-89.719,26.394c-10.377,3.038-21.111,4.591-31.906,4.591h-6.238     c-16.41,0-32.529-4.463-46.609-12.911c-7.381-4.429-13.815-11.418-19.661-21.367c-6.118-10.436-17.408-16.922-29.448-16.922     C15.309,106.667,0,121.975,0,140.8v196.267c0,15.864,10.923,29.107,25.6,32.922V396.8c0,4.719,3.823,8.533,8.533,8.533     c4.71,0,8.533-3.814,8.533-8.533v-26.812c14.677-3.814,25.6-17.058,25.6-32.922V320c0-23.526,19.14-42.667,42.667-42.667H230.4     c9.114,0,18.057,2.415,25.847,6.98c18.569,10.871,27.639,34.039,22.059,56.354L257.869,422.4H34.133     C15.309,422.4,0,437.709,0,456.533c0,18.825,15.309,34.133,34.133,34.133h443.733c18.825,0,34.133-15.309,34.133-34.133     C512,437.709,496.691,422.4,477.867,422.4z M119.467,192h51.2c4.71,0,8.533,3.814,8.533,8.533s-3.823,8.533-8.533,8.533h-51.2     c-4.71,0-8.533-3.814-8.533-8.533S114.756,192,119.467,192z M204.8,243.2h-85.333c-4.71,0-8.533-3.814-8.533-8.533     s3.823-8.533,8.533-8.533H204.8c4.71,0,8.533,3.814,8.533,8.533S209.51,243.2,204.8,243.2z M298.667,38.4h17.067v34.133h-17.067     V38.4z M341.333,388.267c-4.71,0-8.533-3.823-8.533-8.533s3.823-8.533,8.533-8.533c4.71,0,8.533,3.823,8.533,8.533     S346.044,388.267,341.333,388.267z M375.467,388.267c-4.71,0-8.533-3.823-8.533-8.533s3.823-8.533,8.533-8.533     c4.71,0,8.533,3.823,8.533,8.533S380.177,388.267,375.467,388.267z M341.333,277.333c-23.526,0-42.667-19.14-42.667-42.667     c0-23.526,19.14-42.667,42.667-42.667C364.86,192,384,211.14,384,234.667C384,258.193,364.86,277.333,341.333,277.333z" fill="#e24545"/>
                                                <path d="M341.333,209.067c-14.114,0-25.6,11.486-25.6,25.6s11.486,25.6,25.6,25.6c14.114,0,25.6-11.486,25.6-25.6     S355.447,209.067,341.333,209.067z M341.333,243.2c-4.71,0-8.533-3.823-8.533-8.533s3.823-8.533,8.533-8.533     c4.71,0,8.533,3.823,8.533,8.533S346.044,243.2,341.333,243.2z" fill="#e24545"/>
                                                <rect x="460.799" y="294.4" width="51.2" height="17.067" fill="#e24545"/>
                                                <path d="M486.4,243.2c-14.114,0-25.6,11.486-25.6,25.6v8.533H512V268.8C512,254.686,500.514,243.2,486.4,243.2z" fill="#e24545"/>
                                                <path d="M460.8,371.2c0,14.114,11.486,25.6,25.6,25.6s25.6-11.486,25.6-25.6v-8.533h-51.2V371.2z" fill="#e24545"/>
                                                <rect x="460.799" y="328.533" width="51.2" height="17.067" fill="#e24545"/>
                                            </svg>
                                            <b class="text">Собственная фабрика в Иваново</b>
                                            <span class="des">С нами выгодно сотрудничать</span>
                                        </a>
                                    </div>
                                    <?php if($theme_option['header-is-search']){ ?>
                                        <div class="header-custom-search-box">
                                            <?php get_search_form(); ?>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="col-sm-4">
                                    <div class="box box1">
                                    <div class="title boxvitol3" align="right"><span class="text kontacts1"><?php if(isset($theme_option['header_topbar_text']) && $theme_option['header_topbar_text']!='') echo $theme_option['header_topbar_text']; ?></span><span class="des kontacts2">ampirtex37@yandex.ru</span><a href="javascript:void(0)" class="d_show_popup call-back-btn">Заказать звонок</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?// end custom?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php /* custom  
    <div class="header-custom">
        <div class="container">
            <h2>Минимальная сумма заказа - 7 000 руб.</h2>
        </div>
    </div> */?>

    <div class="nav-container">
        <div class="container">
            <div class="nav-inner">
                <div class="toggle-menu">
                    <a href="javascript:;" class="off-canvas-toggle icon-toggle" data-uk-offcanvas="{target:'#pgl-off-canvas'}">
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
                <?php pgl_megamenu(array(
                    'theme_location' => 'mainmenu',
                    'container_class' => 'collapse navbar-collapse navbar-ex1-collapse pull-left',
                    'menu_class' => 'nav navbar-nav megamenu',
                    'show_toggle' => false
                )); ?>
                
                <div class="header_absolute_buttons">
                
                    <?php if( PLG_WOOCOMMERCE_ACTIVED ){ ?>
                        <div class="button_in_nav my_account_btn">
                            <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" >
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <span class="text"><?= ( is_user_logged_in() )? 'Личный кабинет' : 'Личный кабинет'; ?></span>
                            </a>
                        </div>

                    <?php } ?>
                    <?php if( PLG_WOOCOMMERCE_ACTIVED && $theme_option['header-is-cart'] ){ ?>
                        <div class="button_in_nav my_shoppingcart <?= ($woocommerce->cart->cart_contents_count > 0)? 'active' : '' ?>" >
                            <a href="javascript:;" data-uk-offcanvas="{target:'#pgl_cart_canvas'}">
                                <span class="cart_icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="33" height="29" viewBox="0 0 59 51.63">
                                      <path id="_" data-name="" class="cls-1" d="M834.1,2072.29a5.429,5.429,0,0,1,1.21.69,2.815,2.815,0,0,1,.922,1.09,8.3,8.3,0,0,1,.461,1.33,2.333,2.333,0,0,1-.173,1.5l-7.26,25.81c-0.231.39-.443,0.73-0.634,1.04a3.35,3.35,0,0,1-.806.86,3.616,3.616,0,0,1-1.038.58,3.988,3.988,0,0,1-1.21.17H794.346a3.8,3.8,0,0,1-1.383-.23,3.6,3.6,0,0,1-1.095-.69,3.411,3.411,0,0,1-.806-1.04,9.327,9.327,0,0,1-.519-1.27l-3.111-26.27h-6.108a3.434,3.434,0,0,1-1.382-.29,3.141,3.141,0,0,1-1.153-.8,6.082,6.082,0,0,1-.806-1.16,4.6,4.6,0,0,1,0-2.88,6.337,6.337,0,0,1,.806-1.15,3.163,3.163,0,0,1,1.153-.81,3.6,3.6,0,0,1,1.382-.28h9.334a3.866,3.866,0,0,1,1.268.23,2.394,2.394,0,0,1,1.095.69c0.268,0.31.536,0.65,0.806,1.03a2.185,2.185,0,0,1,.4,1.27l0.576,4.15h34.34l0.345-1.04a2.451,2.451,0,0,1,.634-1.26,4.7,4.7,0,0,1,1.1-.93,3.671,3.671,0,0,1,1.325-.46,3.175,3.175,0,0,1,1.441.12H834.1Zm-26.965,18.32h7.49l-0.115-3.69h-7.26l-0.115,3.69h0Zm7.49,3.69h-7.49l0.115,3.69h7.26l0.115-3.69h0Zm-7.49-11.06h7.49l-0.115-3.69h-7.26l-0.115,3.69h0Zm-11.869-3.69,0.346,3.69h7.951l-0.116-3.69h-8.181Zm0.806,7.37,0.577,3.69h6.8l0.116-3.69h-7.491Zm1.038,7.38,0.345,3.69h6.108l-0.116-3.69h-6.337Zm25.7,3.69,1.152-3.69H818.2l0.116,3.69h4.494Zm2.189-7.38,0.922-3.69h-7.6l-0.116,3.69H825Zm1.959-7.37,1.268-3.69H818.2l0.116,3.69h8.642ZM792.5,2114.58a3.659,3.659,0,0,0,.4,2.13,8.271,8.271,0,0,0,1.209,1.73,5.213,5.213,0,0,0,1.729,1.21,5.025,5.025,0,0,0,2.074.46,4.815,4.815,0,0,0,2.19-.46,7.481,7.481,0,0,0,1.728-1.21,4.931,4.931,0,0,0,1.21-1.73,4.63,4.63,0,0,0,0-4.26,4.931,4.931,0,0,0-1.21-1.73,7.481,7.481,0,0,0-1.728-1.21,4.815,4.815,0,0,0-2.19-.46,5.025,5.025,0,0,0-2.074.46,5.213,5.213,0,0,0-1.729,1.21,8.271,8.271,0,0,0-1.209,1.73,3.659,3.659,0,0,0-.4,2.13h0Zm22.01,0a7.39,7.39,0,0,0,.519,2.13,4.89,4.89,0,0,0,1.209,1.73,7.423,7.423,0,0,0,1.729,1.21,4.8,4.8,0,0,0,2.189.46,5.029,5.029,0,0,0,2.075-.46,5.208,5.208,0,0,0,1.728-1.21,8.275,8.275,0,0,0,1.21-1.73,5.83,5.83,0,0,0,0-4.26,8.275,8.275,0,0,0-1.21-1.73,5.208,5.208,0,0,0-1.728-1.21,5.029,5.029,0,0,0-2.075-.46,4.8,4.8,0,0,0-2.189.46,7.423,7.423,0,0,0-1.729,1.21,4.89,4.89,0,0,0-1.209,1.73,7.39,7.39,0,0,0-.519,2.13h0Z" transform="translate(-777.75 -2068.5)"/>
                                    </svg>
                                    <span class="count"><?php echo $woocommerce->cart->cart_contents_count; ?></span>
                                </span>
                                <span class="text">Моя корзина</span>
                            </a>
                        </div>
                    <?php } ?>

                </div><!-- /header_absolute_buttons-->
            </div>
        </div>
    </div>
    <?php do_action('pgl_after_header'); ?>


    <!-- callback - popup -->
    <div id="d_popup_container" class="popup_container" style="display: none">
        <div class="d_popup_inner">
            <span class="d_close_popup">x</span>
            <div class="form-call-back">
                <?php echo do_shortcode('[contact-form-7 id="3565" title="Заказать звонок"]'); ?>
            </div>
        </div>
    </div>
    <!-- special offer - popup -->
    <div id="d_special_offer_popup_container" class="popup_container" style="display: none">
        <div class="d_popup_inner special_offer_popup_inner">
            <span class="d_close_popup">x</span>
            <div class="form-call-back">
                <?php echo do_shortcode('[contact-form-7 id="23663" title="Не нашли что искали?"]'); ?>
            </div>
        </div>
    </div> 
    <script>
        jQuery(document).ready(function(){

            jQuery('.d_show_popup').click(function(){
                jQuery('#d_popup_container').fadeIn();
            });

            jQuery('.d_close_popup').click(function(){
                jQuery(this).closest('.popup_container').fadeOut();
            });

            jQuery('.popup_container').click(function(e){
                if(e.target !== e.currentTarget) return;
                jQuery(this).fadeOut();
            });

            //show popup on timeout
            var offer_first_run;
            clearTimeout(offer_first_run);
            offer_first_run = setTimeout(function(){
                jQuery('#d_special_offer_popup_container').fadeIn();
                clearTimeout(offer_first_run);
            }, 45 * 1000); //10 sec

            //show popup on leaving page
            jQuery(document).mouseleave(function () {
                jQuery('#d_special_offer_popup_container').fadeIn();
                clearTimeout(offer_first_run);
            });

        });
    </script>
    <!-- /popup -->

</header>
<!-- //HEADER -->