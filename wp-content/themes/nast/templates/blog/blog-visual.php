<?php //get_template_part( 'templates/single/meta' ); ?>
<div class="blog-container-inner">
    <div class="post-thumb">
        <?php the_post_thumbnail('blog-list'); ?>
        <span class="published"> 
            <?php the_time( 'M d Y' ); ?>
        </span>
    </div>
    <div class="visual-inner">
        <h2 class="blog-title">
            <a href="<?php the_permalink(); ?>">
                <?php the_title(); ?>
            </a>
        </h2>
        <div class="blog-content">
            <?php echo pgl_get_excerpt(15,'...'); ?>
            <a href="<?php the_permalink(); ?>"><?php _e('Read more','nast'); ?></a>
        </div>
        <div class="meta">
            <span class="author-link">
                <i class="fa fa-edit"></i>
                <?php the_author_posts_link(); ?>
            </span>
            <span class="comment-count">
                <i class="fa fa-comment-o"></i>
                <?php comments_popup_link(__(' 0 comment', 'nast'), __(' 1 comment', 'nast'), __(' % comments', 'nast')); ?>
            </span>
        </div>
    </div>
</div>