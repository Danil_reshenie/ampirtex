<?php

global $theme_option;
?>
        	<footer id="pgl-footer" class="pgl-footer">
                <?php 
                   do_action( 'pgl_footer_layout_style' );
                ?>
                <div class="footer-copyright">
				    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <?php 
                                    if( has_nav_menu( 'footer-menu' ) ){
                                        wp_nav_menu(array(
                                                'theme_location' => 'footer-menu',
                                                'container' => '',
                                                'menu_class' => 'links'
                                            ));
                                    }
                                ?>
                                <div class="copyright">
                                    <?php echo $theme_option['footer-copyright']; ?>
                                </div>
                            </div>
                            <div class="col-sm-6 text-right">
                                <?php 
                                    $footer_payment = '';
                                    if( isset($theme_option['footer-payment']['url']) )
                                        $footer_payment = $theme_option['footer-payment']['url'];
                                    if($footer_payment){
                                ?>
                                <img src="<?php echo esc_url( $theme_option['footer-payment']['url'] ); ?>" alt="">
                                <?php } ?>
                            </div>
                        </div>
				    </div>
				</div>
        	</footer>
        </div><!--  End .wrapper-inner -->
    </div><!--  End .pgl-wrapper -->
    
    <?php do_action('pgl_after_wrapper'); ?>

	<?php wp_footer(); ?>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44915599 = new Ya.Metrika({
                    id:44915599,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44915599" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

</body>
<!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter42163439 = new Ya.Metrika({ id:42163439, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/42163439" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
</html>