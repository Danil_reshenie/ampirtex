<?php 
extract(shortcode_atts(array(
    'title' => '',
    'custom_links' => '',
    'images' => '',
    'el_class' => '',
    'columns_count'=>'5',
    'layout' => 'carousel',
), $atts));

switch ($columns_count) {
	case '5':
		$class_column='col-sm-20 col-xs-6';
		break;
	case '4':
		$class_column='col-sm-3 col-xs-6';
		break;
	case '3':
		$class_column='col-lg-4 col-md-4 col-sm-4 col-xs-6';
		break;
	case '2':
		$class_column='col-lg-6 col-md-6 col-sm-6 col-xs-6';
		break;
	default:
		$class_column='col-lg-12 col-md-12 col-sm-12 col-xs-6';
		break;
}

$custom_links = explode( ',', $custom_links);
$images = explode( ',', $images);

if(count($images)>0){
?>

	<div class="brands <?php echo esc_attr( 'brands_'.$layout ); ?>">
		<div class="row">
			<?php if($layout=='carousel'){ ?>
			<div data-owl="slide" 
                    data-item-slide="<?php echo esc_attr($columns_count); ?>" 
                    data-ow-rtl="<?php echo is_rtl()?'true':'false'; ?>" 
                    class="owl-carousel owl-theme">
                <?php $delay = 0; ?>
				<?php foreach ($images as $key => $image) { ?>
					<div class="wow bounceIn col-sm-12" data-wow-duration="1s" data-wow-delay="<?php echo esc_attr( $delay ); ?>ms">
						<?php 
							$img = wpb_getImageBySize(array( 'attach_id' => $image, 'thumb_size' => 'full' ));
							$link_start = $link_end = '';
							if ( isset( $custom_links[$key] ) && $custom_links[$key] != '' ) {
						        $link_start = '<a href="'.$custom_links[$key].'">';
						        $link_end = '</a>';
						    }
						    echo $link_start.$img['thumbnail'].$link_end;
						?>
					</div>
					<?php $delay+=300; ?>
				<?php } ?>
            </div>
            <?php }else{ ?>
				<?php $delay = 0; ?>
				<?php foreach ($images as $key => $image) { ?>
					<div class="wow bounceIn <?php echo esc_attr( $class_column ); ?>" data-wow-duration="1s" data-wow-delay="<?php echo esc_attr( $delay ); ?>ms">
						<?php 
							$img = wpb_getImageBySize(array( 'attach_id' => $image, 'thumb_size' => 'full' ));
							$link_start = $link_end = '';
							if ( isset( $custom_links[$key] ) && $custom_links[$key] != '' ) {
						        $link_start = '<a href="'.$custom_links[$key].'">';
						        $link_end = '</a>';
						    }
						    echo $link_start.$img['thumbnail'].$link_end;
						?>
					</div>
					<?php $delay+=300; ?>
				<?php } ?>
            <?php } ?>
		</div>
	</div>
<?php
}
