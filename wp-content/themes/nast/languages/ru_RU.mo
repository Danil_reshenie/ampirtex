��          �   %   �      @  K   A     �     �     �  	   �     �     �     �     �  	             (     -  )   6     `  	   t     ~     �     �     �     �     �     �     �  b  �  B   [     �     �     �     �     �  #   �  
   !     ,     G     [     q     �  k   �  &   �     "  $   5     Z     v     �  %   �  +   �     �  ,   	                                                        
                                                         	             %1$d = first, %2$d = last, %3$d = totalShowing %1$d–%2$d of %3$d results Additional Information Additional information Address Book Anti-spam Change Password Confirm new password Logout Lost your password? My Orders My Wishlist Page Password Password (leave blank to leave unchanged) Proceed to Checkout Read More Register Remember me Save Save Address Save changes Search results for &ldquo; Shipping Address Username or email address Project-Id-Version: Flatize Fashion Ecommerce v1.1.4
Report-Msgid-Bugs-To: 
POT-Creation-Date: 
PO-Revision-Date: 2017-12-07 15:24+0300
Last-Translator: admin <admin@admin.com>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 2.0.6
X-Poedit-SourceCharset: utf-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
Language: en_GB
X-Poedit-SearchPath-0: .
 %1$d–%2$d из %3$d результатов на странице Свойства Свойства Адреса Анти-спам Изменить пароль Подтвердите пароль Выйти Забыли пароль? Мои заказы Мои желания Страница Пароль Пароль (оставьте поле пустым, если не хотите менять пароль) Перейти к оформлению Подробнее Зарегистрироваться Запомнить меня Сохранить Сохранить адрес Сохранить изменения Результаты поиска "&ldquo;" Адрес доставки Имя пользователя или email 