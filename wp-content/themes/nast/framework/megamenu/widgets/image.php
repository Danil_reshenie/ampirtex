<?php

	class PGL_Shortcode_Image extends PGL_Shortcode_Base{

		public function __construct( ){
			// add hook to convert shortcode to html.
			$this->name = str_replace( 'pgl_shortcode_','',strtolower( __CLASS__ ) );
			$this->key = 'pgl_'.$this->name;
			$this->excludedMegamenu = true;
			parent::__construct( );
		}
		
		public function getButton( $data=null ){
			$button = array(
				'icon'	 => 'image',
				'title' => $this->l( 'Single Image' ),
				'desc'  => $this->l( 'Display Banner Image Or Ads Banner' ),
				'name'  => $this->name
			);

			return $button;
		}

		public function getOptions( ){
		    $this->options[] = array(
		        'label' 	=> __('Image', 'nast'),
		        'id' 		=> 'image',
		        'type' 		=> 'image',
		        'default'	=> '',
		        'hint'		=> '',
		        );
		   $this->options[] = array(
		        'label' 	=> __('Link Image', 'nast'),
		        'id' 		=> 'link',
		        'type' 		=> 'text',
		        'default'	=> '',
	        );

		    $this->options[] = array(
		        'label' 	=> __('Addition Class', 'nast'),
		        'id' 		=> 'class',
		        'type' 		=> 'text',
		        'explain'	=> __( 'Using to make own style', 'nast' ),
		        'default'	=> '',
	        );
		}
	}
?>