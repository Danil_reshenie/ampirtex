<?php
/*==========================================================================
Setup Theme
==========================================================================*/
function plg_theme_setup(){
    load_theme_textdomain( 'nast', get_template_directory().'/languages' );
    register_nav_menus( array(
        'mainmenu'   => __( 'Main Menu', 'nast' ),
        'footer-menu' => __('Copyright Menu', 'nast'),
    ) );

    add_theme_support( 'automatic-feed-links' );

    add_theme_support( 'post-formats', array(
       'image', 'video', 'audio', 'gallery', 'status'
    ) );
    add_theme_support( "post-thumbnails" );
    add_image_size( 'blog-mini',400,400,true );
    add_image_size( 'blog-list',600,280,true );

    if ( ! isset( $content_width ) ) $content_width = 900;
    add_theme_support( 'custom-header' );
    add_theme_support( 'custom-background' );
    add_theme_support( "title-tag" );
}
add_action( 'after_setup_theme', 'plg_theme_setup' );

/*==========================================================================
Require Plugins
==========================================================================*/
add_filter( 'pgl_list_plugins_required' , 'pgl_list_plugins_required' );
function pgl_list_plugins_required($list){
    $path_link = 'http://aztheme.com/plugins/';
    $path = PGL_FRAMEWORK_PATH . 'plugins/';
    $list[] = array(
                'name'                     => 'WooCommerce', // The plugin name
                'slug'                     => 'woocommerce', // The plugin slug (typically the folder name)
                'required'                 => true, // If false, the plugin is only 'recommended' instead of required
            );
    $list[] = array(
                'name'                     => 'Redux Framework', // The plugin name
                'slug'                     => 'redux-framework', // The plugin slug (typically the folder name)
                'required'                 => true, // If false, the plugin is only 'recommended' instead of required
            );

    // $list[] = array(
    //             'name'                     => 'Codestyling Localization', // The plugin name
    //             'slug'                     => 'codestyling-localization', // The plugin slug (typically the folder name)
    //             'required'                 => false, // If false, the plugin is only 'recommended' instead of required
    //         );
    
    $list[] = array(
                'name'                     => 'Contact Form 7', // The plugin name
                'slug'                     => 'contact-form-7', // The plugin slug (typically the folder name)
                'required'                 => true, // If false, the plugin is only 'recommended' instead of required
            );
    $list[] = array(
                'name'                     => 'WPBakery Visual Composer', // The plugin name
                'slug'                     => 'js_composer', // The plugin slug (typically the folder name)
                'required'                 => true,
                'source'                   => $path_link . 'js_composer.zip', // The plugin source
            );
    $list[] = array(
                'name'                     => 'Revolution Slider', // The plugin name
                'slug'                     => 'revslider', // The plugin slug (typically the folder name)
                'required'                 => true, // If false, the plugin is only 'recommended' instead of required
                'source'                   => $path_link . 'revslider.zip', // The plugin source
            );
    $list[] = array(
                'name'                     => 'YITH WooCommerce Zoom Magnifier', // The plugin name
                'slug'                     => 'yith-woocommerce-zoom-magnifier', // The plugin slug (typically the folder name)
                'required'                 =>  true
            );
    $list[] = array(
            'name'                     => 'YITH WooCommerce Wishlist', // The plugin name
            'slug'                     => 'yith-woocommerce-wishlist', // The plugin slug (typically the folder name)
            'required'                 => true
        );

    $list[] = array(
            'name'                     => 'YITH Woocommerce Compare', // The plugin name
            'slug'                     => 'yith-woocommerce-compare', // The plugin slug (typically the folder name)
            'required'                 => true
        );
    $list[] = array(
            'name'                     => 'Image Widget', // The plugin name
            'slug'                     => 'image-widget', // The plugin slug (typically the folder name)
            'required'                 => false
        );
    $list[] = array(
            'name'                     => 'PGL Framework', // The plugin name
            'slug'                     => 'pgl_framework', // The plugin slug (typically the folder name)
            'required'                 => true,
            'source'                   => $path . 'pgl_framework.zip', // The plugin source
        );
    return $list;
}

/*==========================================================================
Header Sticky
==========================================================================*/
add_action( 'wp_footer', 'init_header_sticky' );
function init_header_sticky(){
    global $theme_option,$woocommerce;
    $login_url = wp_login_url();
    $myaccount_page_id = get_option( 'woocommerce_myaccount_page_id' );
    if ( $myaccount_page_id ) {
        $login_url = get_permalink( $myaccount_page_id );
        if ( get_option( 'woocommerce_force_ssl_checkout' ) == 'yes' ) {
            $login_url = str_replace( 'http:', 'https:', $login_url );
        }
    }
    if(!$theme_option['header-is-sticky']) return;
?>
    <div class="header-sticky">
        <div class="container">
            <div class="header-sticky-inner">
                <div class="row">
                    <div class="col-xs-3 logo">
                        <?php do_action('pgl_set_logo'); ?>
                    </div>
                    <div class="col-xs-9 sticky-action">
                        <?php if(PLG_WOOCOMMERCE_ACTIVED && $theme_option['header-is-cart-sticky']){ ?>
                        <div class="shoppingcart  <?= ($woocommerce->cart->cart_contents_count > 0)? 'active' : '' ?>">
                            <a href="javascript:;" data-uk-offcanvas="{target:'#pgl_cart_canvas'}" style="background:none;">
                                <svg xmlns="http://www.w3.org/2000/svg" width="22" height="20" viewBox="0 0 59 51.63">
                                      <path id="_" data-name="" class="cls-1" d="M834.1,2072.29a5.429,5.429,0,0,1,1.21.69,2.815,2.815,0,0,1,.922,1.09,8.3,8.3,0,0,1,.461,1.33,2.333,2.333,0,0,1-.173,1.5l-7.26,25.81c-0.231.39-.443,0.73-0.634,1.04a3.35,3.35,0,0,1-.806.86,3.616,3.616,0,0,1-1.038.58,3.988,3.988,0,0,1-1.21.17H794.346a3.8,3.8,0,0,1-1.383-.23,3.6,3.6,0,0,1-1.095-.69,3.411,3.411,0,0,1-.806-1.04,9.327,9.327,0,0,1-.519-1.27l-3.111-26.27h-6.108a3.434,3.434,0,0,1-1.382-.29,3.141,3.141,0,0,1-1.153-.8,6.082,6.082,0,0,1-.806-1.16,4.6,4.6,0,0,1,0-2.88,6.337,6.337,0,0,1,.806-1.15,3.163,3.163,0,0,1,1.153-.81,3.6,3.6,0,0,1,1.382-.28h9.334a3.866,3.866,0,0,1,1.268.23,2.394,2.394,0,0,1,1.095.69c0.268,0.31.536,0.65,0.806,1.03a2.185,2.185,0,0,1,.4,1.27l0.576,4.15h34.34l0.345-1.04a2.451,2.451,0,0,1,.634-1.26,4.7,4.7,0,0,1,1.1-.93,3.671,3.671,0,0,1,1.325-.46,3.175,3.175,0,0,1,1.441.12H834.1Zm-26.965,18.32h7.49l-0.115-3.69h-7.26l-0.115,3.69h0Zm7.49,3.69h-7.49l0.115,3.69h7.26l0.115-3.69h0Zm-7.49-11.06h7.49l-0.115-3.69h-7.26l-0.115,3.69h0Zm-11.869-3.69,0.346,3.69h7.951l-0.116-3.69h-8.181Zm0.806,7.37,0.577,3.69h6.8l0.116-3.69h-7.491Zm1.038,7.38,0.345,3.69h6.108l-0.116-3.69h-6.337Zm25.7,3.69,1.152-3.69H818.2l0.116,3.69h4.494Zm2.189-7.38,0.922-3.69h-7.6l-0.116,3.69H825Zm1.959-7.37,1.268-3.69H818.2l0.116,3.69h8.642ZM792.5,2114.58a3.659,3.659,0,0,0,.4,2.13,8.271,8.271,0,0,0,1.209,1.73,5.213,5.213,0,0,0,1.729,1.21,5.025,5.025,0,0,0,2.074.46,4.815,4.815,0,0,0,2.19-.46,7.481,7.481,0,0,0,1.728-1.21,4.931,4.931,0,0,0,1.21-1.73,4.63,4.63,0,0,0,0-4.26,4.931,4.931,0,0,0-1.21-1.73,7.481,7.481,0,0,0-1.728-1.21,4.815,4.815,0,0,0-2.19-.46,5.025,5.025,0,0,0-2.074.46,5.213,5.213,0,0,0-1.729,1.21,8.271,8.271,0,0,0-1.209,1.73,3.659,3.659,0,0,0-.4,2.13h0Zm22.01,0a7.39,7.39,0,0,0,.519,2.13,4.89,4.89,0,0,0,1.209,1.73,7.423,7.423,0,0,0,1.729,1.21,4.8,4.8,0,0,0,2.189.46,5.029,5.029,0,0,0,2.075-.46,5.208,5.208,0,0,0,1.728-1.21,8.275,8.275,0,0,0,1.21-1.73,5.83,5.83,0,0,0,0-4.26,8.275,8.275,0,0,0-1.21-1.73,5.208,5.208,0,0,0-1.728-1.21,5.029,5.029,0,0,0-2.075-.46,4.8,4.8,0,0,0-2.189.46,7.423,7.423,0,0,0-1.729,1.21,4.89,4.89,0,0,0-1.209,1.73,7.39,7.39,0,0,0-.519,2.13h0Z" transform="translate(-777.75 -2068.5)"/>
                                </svg>
                                <span class="count"><?php echo $woocommerce->cart->cart_contents_count; ?></span>
                                <span class="text"><?php echo __('Shopping Bag','nast'); ?></span>
                            </a>
                        </div>
                        <?php } ?>
                        <?php if($theme_option['header-is-search-sticky']){ ?>
                        <div class="search-form">
                            <div class="icon-search"><i class="fa fa-search"></i></div>
                            <div class="box-search">
                                <div class="box-search-info">
                                    <?php get_search_form(); ?>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        jQuery(document).ready(function($) {
            $('.header-sticky .sticky-action').append($('#pgl-mainnav').clone());
            var $menu_action = $('#pgl-mainbody').offset().top;
            $(window).scroll(function(event) {
                if( $(document).scrollTop() > $menu_action ){
                    $('.header-sticky').addClass('fixed');
                }else{
                    $('.header-sticky').removeClass('fixed');
                }
            });
        });
    </script>
<?php
}

/*==========================================================================
Styles & Scripts
==========================================================================*/
function init_styles_scripts(){
	$protocol = is_ssl() ? 'https:' : 'http:';

    wp_enqueue_script("jquery");
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ){
  		wp_enqueue_script( 'comment-reply' );
	}

    wp_enqueue_script( 'wp-color-picker' );
    wp_enqueue_style( 'wp-color-picker' );
    
	// Add Google Font
    wp_enqueue_style('theme-questrial-font',$protocol.'//fonts.googleapis.com/css?family=Questrial');
    wp_enqueue_style('theme-montserrat-font',$protocol.'//fonts.googleapis.com/css?family=Montserrat+Alternates:400,700');

	// Css 
    if(is_rtl()){
        wp_enqueue_style('theme-bootstrap',PGL_THEME_URI.'/css/bootstrap-rtl.css',array(),PGL_THEME_VERSION);
    }else{
        wp_enqueue_style('theme-bootstrap',PGL_THEME_URI.'/css/bootstrap.css',array(),PGL_THEME_VERSION);
    }
	wp_enqueue_style('theme-font-awesome',PGL_THEME_URI.'/css/font-awesome.min.css',array(),PGL_THEME_VERSION);
	wp_enqueue_style('theme-animate',PGL_THEME_URI.'/css/animate.css',array(),PGL_THEME_VERSION);
    wp_enqueue_style('theme-magnific',PGL_THEME_URI.'/css/magnific-popup.css',array(),PGL_THEME_VERSION);
    if(PLG_WOOCOMMERCE_ACTIVED)
	   wp_enqueue_style('theme-woocommerce-css',PGL_THEME_URI.'/css/woocommerce.css',array(),PGL_THEME_VERSION);

    //Owl Carousel Assets
    wp_enqueue_style('owl-carousel-base',PGL_THEME_URI.'/owl-carousel/owl.carousel.css',array(),PGL_THEME_VERSION);
    wp_enqueue_style('owl-carousel-theme',PGL_THEME_URI.'/owl-carousel/owl.theme.css',array(),PGL_THEME_VERSION);
    wp_enqueue_style('owl-carousel-transitions',PGL_THEME_URI.'/owl-carousel/owl.transitions.css',array(),PGL_THEME_VERSION);
   
    wp_enqueue_style( 'theme-style', get_stylesheet_uri() );
	// Scripts

    wp_register_script('theme-gmap-core', $protocol .'//maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places',array(),PGL_THEME_VERSION,true );
    wp_register_script('theme-gmap-api',PGL_THEME_URI.'/js/gmaps.js',array(),PGL_THEME_VERSION,array(),PGL_THEME_VERSION,true);

	wp_enqueue_script('theme-bootstrap',PGL_THEME_URI.'/js/bootstrap.min.js',array(),PGL_THEME_VERSION);
    wp_enqueue_script('theme-magnific-popup',PGL_THEME_URI.'/js/jquery.magnific-popup.js',array(),PGL_THEME_VERSION,true);
    wp_enqueue_script('owl-carousel_js',PGL_THEME_URI.'/owl-carousel/owl.carousel.js',array(),PGL_THEME_VERSION);
	wp_enqueue_script('theme-magnific_js',PGL_THEME_URI.'/js/jquery.parallax-1.1.3.js',array(),PGL_THEME_VERSION,true);
	wp_enqueue_script('theme-wow_js',PGL_THEME_URI.'/js/jquery.wow.min.js',array(),PGL_THEME_VERSION,true);
	wp_enqueue_script('theme-modernizr_js',PGL_THEME_URI.'/js/modernizr.custom.js',array(),PGL_THEME_VERSION,true);
    wp_enqueue_script('theme-uk_js',PGL_THEME_URI.'/js/uikit.min.js',array(),PGL_THEME_VERSION,true);
	wp_enqueue_script('theme-main_js',PGL_THEME_URI.'/js/main.js',array(),PGL_THEME_VERSION,true);
	
}
add_action( 'wp_enqueue_scripts','init_styles_scripts' );


/*==========================================================================
Single Post
==========================================================================*/
add_action('pgl_post_before_content','pgl_set_post_thumbnail',10);

add_action('pgl_post_after_content','pgl_single_sharebox',10);
add_action('pgl_post_after_content','pgl_single_related_post',15);
add_action('pgl_post_after_content','pgl_single_author_bio',20);

function pgl_set_post_thumbnail(){
    global $post;
    $postid = $post->ID;
    $link_embed = get_post_meta($postid,'_pgl_post_video',true);
    $gallery = get_post_meta( $postid,'_pgl_post_gallery', true );
    $status = get_post_meta( $postid, '_pgl_post_status' , true );
    $is_thumb = false;
    $content = $output = $start = $end = '';
    
    if( has_post_format( 'video' ) && $link_embed!='' ){
        $content ='<div class="video-responsive">'.wp_oembed_get($link_embed).'</div>';
        $is_thumb = true;
    }else if ( has_post_format( 'audio' ) ){
        $content ='<div class="audio-responsive">'.wp_oembed_get($link_embed).'</div>';
        $is_thumb = true;
    }else if ( has_post_format( 'gallery' ) && $gallery != '' ){
        $count = 0;
        $content =  '<div id="post-slide-'.$postid.'" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">';
        foreach ($gallery as $key => $id){
            $img_src = wp_get_attachment_image_src($key, apply_filters( 'pgl_gallery_image_size','full' ));
            $content.='<div class="item '.(($count==0)?'active':'').'">
                        <img src="'.$img_src[0].'">
                    </div>';
            $count++;
        }
        $content.='</div>
            <a class="left carousel-control" href="#post-slide-'.esc_attr($postid).'" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
            <a class="right carousel-control" href="#post-slide-'.esc_attr($postid).'" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
        </div>';
        $is_thumb = true;
    }else if( has_post_format( 'status' ) && $status != '' ){
        $content ='<div class="status-responsive">'.$status.'</div>';
        $is_thumb = true;
    }else if( has_post_thumbnail() ){
        $content = get_the_post_thumbnail( $postid, apply_filters( 'pgl_single_image_size','full' ) );
        $is_thumb = true;
    }

    if( $is_thumb ){
        $start = '<div class="post-thumb">';
        $end = '</div>';
    }

    $output = $start.$content.$end;
    echo $output;
}

function pgl_single_sharebox(){
    ?>
    <div class="post-share">
        <div class="row">
            <div class="col-sm-4">
                <h4 class="heading"><?php echo __( 'Share this Post!','nast' ); ?></h4>
            </div>
            <div class="col-sm-8">
                <?php get_template_part( 'templates/sharebox' ); ?>
            </div>
        </div>
    </div>
    <?php
}
function pgl_single_related_post(){
    get_template_part('templates/single/related');
}
function pgl_single_author_bio(){
    ?>
    <div class="author-about">
        <?php get_template_part('templates/single/author-bio'); ?>
    </div>
    <?php
}

/*==========================================================================
Language Flags
==========================================================================*/

function pgl_language_flags() {
    
    $language_output = '<select id="pgl-language-switch">';
    
    if (function_exists('icl_get_languages')) {
        $languages = icl_get_languages('skip_missing=0&orderby=code');
        if(!empty($languages)){
            foreach($languages as $l){
                if($l['country_flag_url']){
                    if(!$l['active']) {
                        $language_output .= '<option value="'.$l['url'].'" selected="selected">'.$l['translated_name'].'</option>'."\n";
                    } else {
                        $language_output .= '<option value="'.$l['url'].'" >'.$l['translated_name'].'</option>'."\n";
                    }
                }
            }
        }
    } else {
        $language_output .= '
            <option value="">English</option>
            <option value="">German</option>
            <option value="">Spanish</option>
            <option value="">French</option>
            <option value="">Demo</option>
        ';
    }
    $language_output .= '</select>';
    return $language_output;
}

/*==========================================================================
Sidebar
==========================================================================*/
add_action( 'widgets_init' , 'pgl_sidebar_setup' );
function pgl_sidebar_setup(){
    register_sidebar(array(
        'name'          => __( 'Shop Sidebar','nast' ),
        'id'            => 'shop-sidebar',
        'description'   => __( 'Appears on posts and pages in the sidebar.','nast'),
        'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s"><div class="widget-inner">',
        'after_widget'  => '</div></aside>',
        'before_title'  => '<h3 class="widget-title"><span>',
        'after_title'   => '</span></h3>'
    ));

    register_sidebar(array(
        'name'          => __( 'Shop Single Sidebar','nast' ),
        'id'            => 'shop-single-sidebar',
        'description'   => __( 'Appears on posts and pages in the sidebar.','nast'),
        'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s"><div class="widget-inner">',
        'after_widget'  => '</div></aside>',
        'before_title'  => '<h3 class="widget-title"><span>',
        'after_title'   => '</span></h3>'
    ));

    register_sidebar(array(
        'name'          => __( 'Blog Sidebar','nast' ),
        'id'            => 'blog-sidebar',
        'description'   => __( 'Appears on posts and pages in the sidebar.','nast'),
        'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s"><div class="widget-inner">',
        'after_widget'  => '</div></aside>',
        'before_title'  => '<h3 class="widget-title"><span>',
        'after_title'   => '</span></h3>'
    ));

    register_sidebar(array(
        'name'          => __( 'Visual Composer Sidebar','nast' ),
        'id'            => 'visual-sidebar',
        'description'   => __( 'Appears on posts and pages in the sidebar.','nast'),
        'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s"><div class="widget-inner">',
        'after_widget'  => '</div></aside>',
        'before_title'  => '<h3 class="widget-title"><span>',
        'after_title'   => '</span></h3>'
    ));

    register_sidebar(array(
        'name'          => __( 'Footer 1','nast' ),
        'id'            => 'footer-1',
        'description'   => __( 'Appears on posts and pages in the sidebar.','nast'),
        'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s"><div class="widget-inner">',
        'after_widget'  => '</div></aside>',
        'before_title'  => '<h3 class="widget-title"><span>',
        'after_title'   => '</span></h3>'
    ));
    
    register_sidebar(array(
        'name'          => __( 'Footer 2','nast' ),
        'id'            => 'footer-2',
        'description'   => __( 'Appears on posts and pages in the sidebar.','nast'),
        'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s"><div class="widget-inner">',
        'after_widget'  => '</div></aside>',
        'before_title'  => '<h3 class="widget-title"><span>',
        'after_title'   => '</span></h3>'
    ));
    register_sidebar(array(
        'name'          => __( 'Footer 3','nast' ),
        'id'            => 'footer-3',
        'description'   => __( 'Appears on posts and pages in the sidebar.','nast'),
        'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s"><div class="widget-inner">',
        'after_widget'  => '</div></aside>',
        'before_title'  => '<h3 class="widget-title"><span>',
        'after_title'   => '</span></h3>'
    ));
    register_sidebar(array(
        'name'          => __( 'Footer 4','nast' ),
        'id'            => 'footer-4',
        'description'   => __( 'Appears on posts and pages in the sidebar.','nast'),
        'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s"><div class="widget-inner">',
        'after_widget'  => '</div></aside>',
        'before_title'  => '<h3 class="widget-title"><span>',
        'after_title'   => '</span></h3>'
    ));
    
    register_sidebar(array(
        'name'          => __( 'Footer 5','nast' ),
        'id'            => 'footer-5',
        'description'   => __( 'Appears on posts and pages in the sidebar.','nast'),
        'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s"><div class="widget-inner">',
        'after_widget'  => '</div></aside>',
        'before_title'  => '<h3 class="widget-title"><span>',
        'after_title'   => '</span></h3>'
    ));

}

/*==========================================================================
Header Config
==========================================================================*/

add_filter( 'pgl_header_layout', 'pgl_header_layout_func',100 );
function pgl_header_layout_func(){
    global $theme_option,$wp_query;
    $template = $theme_option['header'];

    if(is_page()){
        $header = get_post_meta( $wp_query->get_queried_object_id(), '_pgl_header_style',true );
        if($header!='global' && $header!=''){
            $template = $header;
        }
    }
    return $template;
}

/*==========================================================================
Action Theme
==========================================================================*/
