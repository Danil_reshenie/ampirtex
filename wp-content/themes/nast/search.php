<?php get_header(); ?>
<?php // pgl_current_page_title_bar(); ?>
<div id="pgl-mainbody" class="container pgl-mainbody">
    <div class="row">
        <!-- MAIN CONTENT -->
        <div id="pgl-main-content" class="pgl-content clearfix <?php echo apply_filters( 'pgl_main_class', '' ); ?>">
            <div class="pgl-content-inner clearfix">
                <h2>Результаты поиска</h2>
            <?php  if ( have_posts() ) : ?>
                <div class="row">
                    <?php while ( have_posts() ) : the_post(); ?>
                        <?php get_template_part( 'templates/search/search'); ?>
                    <?php endwhile; ?>
                </div>
            <?php else : ?>
                <?php get_template_part( 'templates/none' ); ?>
            <?php endif; ?>
            </div>
            <?php pgl_pagination($prev = '<i class="fa fa-angle-left"></i>', $next = '<i class="fa fa-angle-right"></i>'); ?>
        </div>
        <!-- //END MAINCONTENT -->
        <?php do_action('pgl_sidebar_render'); ?>
    </div>
</div>

<?php get_footer(); ?>