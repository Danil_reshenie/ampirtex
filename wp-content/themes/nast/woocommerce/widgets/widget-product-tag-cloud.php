<?php
/**
 * Tag Cloud Widget
 *
 * @author 		WooThemes
 * @category 	Widgets
 * @package 	WooCommerce/Widgets
 * @version 	2.1.0
 * @extends 	WC_Widget
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class PGL_Widget_Product_Tag_Cloud extends WC_Widget_Product_Tag_Cloud {

}

register_widget( 'PGL_Widget_Product_Tag_Cloud' );