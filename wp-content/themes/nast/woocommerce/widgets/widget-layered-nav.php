<?php
/**
 * Layered Navigation Widget
 *
 * @author 		WooThemes
 * @category 	Widgets
 * @package 	WooCommerce/Widgets
 * @version 	2.1.0
 * @extends 	WC_Widget
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class PGL_Widget_Layered_Nav extends WC_Widget_Layered_Nav {
}

register_widget( 'PGL_Widget_Layered_Nav' );
