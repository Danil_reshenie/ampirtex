<?php
/**
 * Edit account form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.7
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

global $woocommerce, $yith_wcwl;
?>

<?php wc_print_notices(); ?>

<div class="row">
	<div class="my-account-left col-sm-3">
	
		<div class="list-group my-account-group">
			<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>#my-orders" class="list-group-item"><?php _e("My Orders", 'greatshop'); ?></a>
			<?php if ( $downloads = WC()->customer->get_downloadable_products() ) { ?>
				<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>#my-downloads" class="list-group-item"><?php _e("My Downloads", 'greatshop'); ?></a>
			<?php } ?>
			
			<?php if ( class_exists( 'YITH_WCWL_UI' ) ) { ?>
			<a href="<?php echo $yith_wcwl->get_wishlist_url(); ?>" class="list-group-item"><?php _e("My Wishlist", 'greatshop'); ?></a>
			<?php } ?>
<!-- 			<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>#address-book" class="list-group-item"><?php _e("Address Book", 'greatshop'); ?></a> -->
			
			<a href="<?php echo wc_customer_edit_account_url(); ?>" class="active list-group-item"><?php _e("Change Password", 'greatshop'); ?></a>
			
			<a href="<?php echo wp_logout_url(site_url()); ?>" class="list-group-item"><?php _e('Logout','greatshop') ?></a>
		</div>

	</div>
	<div class="col-sm-9">
		<form action="" method="post">
			<?php do_action( 'woocommerce_edit_account_form_start' ); ?>
			<div class="row">
				<div class="form-group col-sm-6">
					<label for="account_first_name"><?php _e( 'First name', 'woocommerce' ); ?> <span class="required">*</span></label>
					<input type="text" class="input-text form-control" name="account_first_name" id="account_first_name" value="<?php esc_attr_e( $user->first_name ); ?>" />
				</div>
				<div class="form-group col-sm-6">
					<label for="account_last_name"><?php _e( 'Last name', 'woocommerce' ); ?> <span class="required">*</span></label>
					<input type="text" class="input-text form-control" name="account_last_name" id="account_last_name" value="<?php esc_attr_e( $user->last_name ); ?>" />
				</div>
			</div>
			<div class="form-group">
				<label for="account_email"><?php _e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input type="email" class="input-text form-control" name="account_email" id="account_email" value="<?php esc_attr_e( $user->user_email ); ?>" />
			</div>
			<div class="form-group">
				<label for="password_1"><?php _e( 'Password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
				<input type="password" class="input-text form-control" name="password_1" id="password_1" />
			</div>
			<div class="form-group">
				<label for="password_2"><?php _e( 'Confirm new password', 'woocommerce' ); ?></label>
				<input type="password" class="input-text form-control" name="password_2" id="password_2" />
			</div>

			<?php do_action( 'woocommerce_edit_account_form' ); ?>

			<button  class="btn btn-primary" name="save_account_details"><?php _e( 'Save changes', 'woocommerce' ); ?></button>

			<?php wp_nonce_field( 'save_account_details' ); ?>
			<input type="hidden" name="action" value="save_account_details" />

			<?php do_action( 'woocommerce_edit_account_form_end' ); ?>
		</form>
	</div>
</div>